#FROM python:3-alpine3.15
FROM python:3.9-slim
WORKDIR /school
COPY . /school
RUN pip3 install -r requirements.txt
EXPOSE 80
#CMD python3 ./main.py
CMD ["python3", "app.py"]