console.log('Going into this function');

// Define the search_id object
search_id = {
    "student": ["username", "contact_no", "email", "department"],
    "admin": ["username", "contact_no", "email"],
    "teacher": ["username", "contact_no", "email", "department"],
    "department": ["department_name", "HOD_name"],
    "subject": ["department_name", "subject_name"],
    "class": ["student_id", "department", "class_name"],
    "events": ["event_name", "event_date", "classes"]
};

// // Get the search button
const searchButton = document.getElementById("searchButton");
var panel = searchButton.value;
console.log("Panel value is: ", panel);

// Get the open button and popup container
const openButton = document.getElementById("openButton");
// Get the popup container
const popupContainer = document.getElementById("popupContainer");

// Get the close button
const closeButton = document.getElementById("closeButton");
// Get the delete_all_button
const deleteAllButton = document.getElementById("delete_all_button");

const deleteMainButton = document.getElementById("delete_main_button");

// // Get the delete_main_button

// Function to open the popup
function openPopup() {
    console.log("Opening popup");
    // Make the background screen semi-transparent
    event.preventDefault();
    popupContainer.style.display = "flex";
}

// Function to close the popup
function closePopup() {
    console.log("Closing popup");
    // Hide the popup
    event.preventDefault();
    popupContainer.style.display = "none";
}

// Add event listener to the delete_all_button
document.getElementById("delete_all_button").addEventListener("click", function (event) {
    // Set the href attribute to the Flask endpoint
    a_href_name = "/admin/deleteall/" + panel;
    this.href = a_href_name;
});

// Add event listeners to the open and close buttons
openButton.addEventListener("click", openPopup);
closeButton.addEventListener("click", closePopup);

// Function to check if any input field is empty
function checkInputData() {
    for (let i = 0; i < input_tags.length; i++) {
        if (input_tags[i].value !== '') {
            return true;
        }
    }

    return false;
}

// Function to submit the form
function submitForm() {
    // const hasInputData = checkInputData();
    const hasInputData = true;
    const form = document.getElementById('search');

    if (hasInputData) {
        console.log('Going to submit form');
        form.action = "/search_data/" + panel;
        form.submit();
    } else {
        alert('Please input a search value.');
    }
}

function close_details() {
    document.getElementById('overlay').style.display = 'none';
}

document.getElementById("showDetails").addEventListener('click', function () {
    all_data = document.getElementById('all_data').value;
    console.log("Result data is: ", all_data);
    let result_dict = eval('(' + all_data + ')');
    // Print type of this result_dict
    console.log(typeof all_data);
    document.getElementById('overlay').style.display = 'flex';

    let tableHtml = "<div style='max-height: 75%; overflow-y: auto;'><table style='border: 1px solid black;'>";
    for (const key in result_dict) {
        if (Object.hasOwnProperty.call(result_dict, key)) {
            const value = result_dict[key];
            if (key === 'photo_link') {
                console.log("Going to add photo: ", key, value);
                tableHtml += `<img class="avatar-img rounded-circle" src="/${value}" alt="User Image" style='height:50px; width:50px; border-radius: 50%';>`;
            }
            else {
                key = key.replace('_', ' ')
                //  Convert first letter of each word to capital
                key = key.charAt(0).toUpperCase() + key.slice(1);
                tableHtml += `<tr><td style='padding: 3px; border-radius: 5px;'>${key}</td><td style='padding: 3px; border-radius: 5px;'>${value}</td></tr>`;
            }
        }
        tableHtml += "</table></div>";
    }

    document.getElementById('userDataTable').innerHTML = tableHtml;
    this.classList.add('active');
    console.log("Going to add active");
    });

// // Add eventlistener to button with id searchButton, where once clicked it will add action to form with id search
searchButton.addEventListener("click", submitForm);